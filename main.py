import meraki
import dotenv
import os
import time
import csv

dotenv.load_dotenv()

# Global variables
apikey = os.getenv("MERAKI_APIKEY")
dashboard = meraki.DashboardAPI(api_key=apikey, suppress_logging=True)
current_time = time.strftime("%D", time.localtime())
script_time_stamp = current_time.replace("/", "-")
org_count = 0
network_count = 0
stable_networks = []
candidate_networks = []
beta_networks = []
ooc_networks = []
stable_version = ""
candidate_version = ""
beta_version = ""

def gather_organizations():
    org_list = dashboard.organizations.getOrganizations()
    return org_list

def gather_networks_in_org(organizationId):
    network_list = dashboard.organizations.getOrganizationNetworks(organizationId=organizationId)
    return network_list

def device_data(organizationName, organizationId, deviceType, runningFirmware):
    data = {
                "organizationName": organizationName,
                "organizationId": organizationId,
                "deviceType": deviceType,
                "runningFirmware": runningFirmware
    }
    return data

def create_file(listName, fileName):
    with open(f"outputs/{fileName}", "w") as file:
        writer = csv.writer(file)
        writer.writerow(["Organization Name", "Organization ID", "Device Type", "Running Firmware"])
        for entry in listName:
            writer.writerow([entry["organizationName"], entry["organizationId"], entry["deviceType"], entry["runningFirmware"]])

if __name__ == "__main__":
    org_list = gather_organizations()

    for org in org_list:
        org_count += 1
        network_list = gather_networks_in_org(org["id"])
        for network in network_list:
            network_count += 1
            print(f"Checking network {network['name']}")
            firmware_response = dashboard.networks.getNetworkFirmwareUpgrades(networkId=network["id"])
            for device_type in firmware_response["products"].items():
                for version in device_type[1]["availableVersions"]:
                    if version["releaseType"] == "stable":
                        stable_version = version["firmware"]
                    elif version["releaseType"] == "candidate":
                        candidate_version = version["firmware"]
                    elif version["releaseType"] == "beta":
                        beta_version = version["firmware"]
                if device_type[1]["currentVersion"]["firmware"] == stable_version:
                    stable_data = device_data(organizationName=org["name"], organizationId=org["id"], deviceType=device_type[0], runningFirmware=device_type[1]["currentVersion"]["firmware"])
                    stable_networks.append(stable_data)
                elif device_type[1]["currentVersion"]["firmware"] == candidate_version:
                    candidate_data = device_data(organizationName=org["name"], organizationId=org["id"], deviceType=device_type[0], runningFirmware=device_type[1]["currentVersion"]["firmware"])
                    candidate_networks.append(candidate_data)
                elif device_type[1]["currentVersion"]["firmware"] == beta_version:
                    beta_data = device_data(organizationName=org["name"], organizationId=org["id"], deviceType=device_type[0], runningFirmware=device_type[1]["currentVersion"]["firmware"])
                    beta_networks.append(beta_data)
                else:
                    ooc_data = device_data(organizationName=org["name"], organizationId=org["id"], deviceType=device_type[0], runningFirmware=device_type[1]["currentVersion"]["firmware"])
                    ooc_networks.append(ooc_data)
    
    print(f"\nHad a look at a total of {network_count} networks")

    print("Writing to files...")
    create_file(listName=stable_networks, fileName=f"stable_{script_time_stamp}.csv")
    create_file(listName=candidate_networks, fileName=f"candidate_{script_time_stamp}.csv")
    create_file(listName=beta_networks, fileName=f"beta_{script_time_stamp}.csv")
    create_file(listName=ooc_networks, fileName=f"out-of-compliance_{script_time_stamp}.csv")
    print("Done")